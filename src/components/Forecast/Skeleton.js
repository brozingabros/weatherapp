import React from 'react';
import { Skeleton } from '@material-ui/lab';
import ForecastItem from './ForecastItem';
import useStyles from './styles';

const forecastDataMock = [
  {
    dt: 123456,
    main: {
      temp: 12
    }
  },
  {
    dt: 987653,
    main: {
      temp: 12
    }
  },
  {
    dt: 124567,
    main: {
      temp: 12
    }
  }
];

const ForecastSkeleton = () => {
  const customStyles = useStyles();
  return (
    <Skeleton animation='wave' className={customStyles.skeleton} variant='text'>
      {forecastDataMock.map(day => (
        <ForecastItem key={day.dt}>{day}</ForecastItem>
      ))}
    </Skeleton>
  );
};

export default ForecastSkeleton;
