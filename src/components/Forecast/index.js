import React, { useContext } from 'react';
import Grid from '@material-ui/core/Grid';
import { DataContext } from '../../context';
import useStyles from './styles';
import ForecastItem from './ForecastItem';
import ForecastSkeleton from './Skeleton';

const Forecast = () => {
  const [response] = useContext(DataContext);
  const isLoaded = response && response.status === 'FETCHED';
  const customStyles = useStyles();

  const getForecast = forecast =>
    forecast.filter((day, index) => {
      switch (index) {
        case 8: // + 1 day
          if (day.dt_txt.includes('21:00:00')) {
            return forecast[index - 1];
          }
          return day;
        case 16: // + 2 day
          if (day.dt_txt.includes('21:00:00')) {
            return forecast[index - 1];
          }
          return day;
        case 24: // + 3 day
          if (day.dt_txt.includes('21:00:00')) {
            return forecast[index - 1];
          }
          return day;
        default:
          return null;
      }
    });

  const forecastData = isLoaded && getForecast(response.data.list);

  return (
    <section>
      <Grid container className={customStyles.forecast} component='ul' justify='center'>
        {forecastData ? (
          forecastData.map(day =>
            forecastData[forecastData.length - 1] === day ? (
              <ForecastItem end key={day.dt}>
                {day}
              </ForecastItem>
            ) : (
              <ForecastItem key={day.dt}>{day}</ForecastItem>
            )
          )
        ) : (
          <ForecastSkeleton />
        )}
      </Grid>
    </section>
  );
};

export default Forecast;
