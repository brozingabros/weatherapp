import makeStyles from '@material-ui/core/styles/makeStyles';

const useStyles = makeStyles(theme => ({
  forecastItem: {
    borderRight: `2px solid ${theme.palette.primary.lighter}`,
    color: theme.palette.primary.main,
    fontWeight: '600',
    listStyle: 'none',
    padding: '0 1rem',
    textAlign: 'center',
    '&:last-child': {
      borderRight: 'none'
    }
  }
}));

export default useStyles;
