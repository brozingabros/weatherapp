import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { convertCtoF } from '../../../utils/convertTemperature';
import useStyles from './styles';
import Temperature from '../../Temperature';
import ForecastDay from '../ForecastDay';
import { DataContext } from '../../../context';
import { TEMP_UNITS } from '../../../constants';

const ForecastItem = ({ children, theme }) => {
  const [data] = useContext(DataContext);
  const customStyles = useStyles(theme);
  const convertTimestampToDay = timestamp => {
    const date = new Date(timestamp * 1000);
    return date.toLocaleDateString('en-US', { weekday: 'short' });
  };

  return (
    <Grid item className={customStyles.forecastItem} component='li'>
      <ForecastDay>{convertTimestampToDay(children.dt)}</ForecastDay>
      <Temperature showUnit size='small'>
        {data.currentUnit === TEMP_UNITS.celsius
          ? Math.round(children.main.temp)
          : Math.round(convertCtoF(children.main.temp))}
      </Temperature>
    </Grid>
  );
};

ForecastItem.defaultProps = {
  theme: {}
};

ForecastItem.propTypes = {
  children: PropTypes.shape({
    dt: PropTypes.number,
    main: PropTypes.shape({
      temp: PropTypes.number
    })
  }).isRequired,
  theme: PropTypes.shape({})
};

export default ForecastItem;
