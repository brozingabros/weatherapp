import React from 'react';
import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box';

const ForecastDay = ({ children }) => {
  return (
    <Box component='p' mb={2} mt={0}>
      {children}
    </Box>
  );
};

ForecastDay.propTypes = {
  children: PropTypes.string.isRequired
};

export default ForecastDay;
