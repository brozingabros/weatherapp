import makeStyles from '@material-ui/core/styles/makeStyles';

const useStyles = makeStyles(() => ({
  forecast: {
    margin: 0,
    padding: 0
  },
  skeleton: {
    display: 'flex'
  }
}));

export default useStyles;
