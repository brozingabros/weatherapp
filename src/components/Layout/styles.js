import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  paper: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      height: '600px',
      margin: 'auto',
      width: '330px'
    }
  }
}));

export default useStyles;
