import React from 'react';
import PropTypes from 'prop-types';
import { Paper } from '@material-ui/core';
import useStyles from './styles';

const Layout = ({ children }) => {
  const customStyles = useStyles();
  return (
    <Paper className={customStyles.paper} elevation={9}>
      {children}
    </Paper>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired
};

export default Layout;
