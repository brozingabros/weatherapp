import React, { useContext, useState } from 'react';
import PropTypes from 'prop-types';
import { Box, Grid } from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import { DataContext } from '../../context';
import { TEMP_UNITS } from '../../constants';
import { convertCtoF, convertFtoC } from '../../utils/convertTemperature';
import Temperature from '../Temperature';
import TemperatureUnit from '../TemperatureUnit';

const TemperatureConverter = ({ children }) => {
  const theme = useTheme();
  const [data, setData] = useContext(DataContext);
  const [currentTemp, setCurrentTemp] = useState(() => Math.round(children));

  const convertTemperature = event => {
    const currentTarget = event.target.innerHTML;

    if (currentTarget === TEMP_UNITS.fahrenheit && data.currentUnit === TEMP_UNITS.celsius) {
      setCurrentTemp(prevState => Math.round(convertCtoF(prevState)));
      setData({ ...data, currentUnit: TEMP_UNITS.fahrenheit });
    }
    if (currentTarget === TEMP_UNITS.celsius && data.currentUnit === TEMP_UNITS.fahrenheit) {
      setCurrentTemp(prevState => Math.round(convertFtoC(prevState)));
      setData({ ...data, currentUnit: TEMP_UNITS.celsius });
    }
  };

  return (
    <Box color={theme.palette.primary.main} component='section'>
      <Grid container wrap='nowrap'>
        <Temperature size='large'>{currentTemp}</Temperature>
        <Grid container direction='column' justify='center' style={{ padding: '0 .9rem' }}>
          <TemperatureUnit
            color={data.currentUnit === TEMP_UNITS.celsius ? theme.palette.primary.main : theme.palette.primary.light}
            component='button'
            onClick={convertTemperature}>
            {TEMP_UNITS.celsius}
          </TemperatureUnit>
          <TemperatureUnit
            color={
              data.currentUnit === TEMP_UNITS.fahrenheit ? theme.palette.primary.main : theme.palette.primary.light
            }
            component='button'
            onClick={convertTemperature}>
            {TEMP_UNITS.fahrenheit}
          </TemperatureUnit>
        </Grid>
      </Grid>
    </Box>
  );
};

TemperatureConverter.propTypes = {
  children: PropTypes.number.isRequired
};

export default TemperatureConverter;
