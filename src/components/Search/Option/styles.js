import makeStyles from '@material-ui/core/styles/makeStyles';

export const useStyles = makeStyles(theme => ({
  locationIcon: {
    color: theme.palette.primary.main
  }
}));
// custom classes overrides for highlighting of the typography component
export const useNoHighlight = makeStyles(() => ({
  body2: {
    fontWeight: 400
  }
}));

export const useHighlight = makeStyles(() => ({
  body2: {
    fontWeight: 700
  }
}));
