import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { Grid, Typography } from '@material-ui/core';
import { LocationOn } from '@material-ui/icons';
import parse from 'autosuggest-highlight/parse';
import { UrlContext } from '../../../context';
import { API } from '../../../constants';
import { useHighlight, useNoHighlight, useStyles } from './styles';

const Option = ({ option }) => {
  const [, setUrl] = useContext(UrlContext);
  const customStyles = useStyles();
  const customStylesNoHighlight = useNoHighlight();
  const customStylesHighlight = useHighlight();

  const handleSearchOptionClick = request => {
    const geocoder = new window.google.maps.Geocoder();
    geocoder.geocode(request, (place, status) => {
      if (status === 'OK') {
        const { lat, lon } = {
          lat: place[0].geometry.location.lat(),
          lon: place[0].geometry.location.lng()
        };
        setUrl(`${API.url}?lat=${lat}&lon=${lon}&${API.units}&${API.key}`);
      }
    });
  };

  const matches = option.structured_formatting.main_text_matched_substrings;
  const parts = parse(
    option.structured_formatting.main_text,
    matches.map(match => [match.offset, match.offset + match.length])
  );

  return (
    <Grid
      container
      alignItems='center'
      spacing={4}
      onClick={() => handleSearchOptionClick({ placeId: option.place_id })}>
      <Grid item>
        <LocationOn className={customStyles.locationIcon} />
      </Grid>
      <Grid item xs>
        {parts.map(part => (
          <Typography
            classes={part.highlight ? customStylesHighlight : customStylesNoHighlight}
            color='primary'
            component='span'
            key={part.text}
            variant='body2'>
            {part.text}
          </Typography>
        ))}
        <Typography color='primary' variant='body2'>
          {option.structured_formatting.secondary_text}
        </Typography>
      </Grid>
    </Grid>
  );
};

Option.propTypes = {
  option: PropTypes.shape({
    structured_formatting: PropTypes.shape({
      main_text: PropTypes.string,
      main_text_matched_substrings: PropTypes.arrayOf(PropTypes.shape({})),
      secondary_text: PropTypes.string
    }),
    place_id: PropTypes.string
  }).isRequired
};

export default Option;
