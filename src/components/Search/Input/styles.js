import makeStyles from '@material-ui/core/styles/makeStyles';

const useStyles = makeStyles(() => ({
  textField: {
    flexGrow: 1
  },
  icon: {
    padding: 0
  }
}));

export default useStyles;
