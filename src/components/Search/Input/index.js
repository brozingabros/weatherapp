import React from 'react';
import PropTypes from 'prop-types';
import { Grid, IconButton, TextField } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import useStyles from './styles';

const Input = ({ params, onBackClick }) => {
  const customStyles = useStyles();
  const placeholder = 'Search location';

  return (
    <Grid container alignItems='center' spacing={2}>
      <Grid item>
        <IconButton
          aria-label='search back button'
          classes={{ root: customStyles.icon }}
          color='inherit'
          onClick={() => onBackClick(false)}>
          <ArrowBackIcon />
        </IconButton>
      </Grid>
      <Grid item className={customStyles.textField}>
        <TextField
          {...params}
          fullWidth
          InputProps={{ ...params.InputProps, disableUnderline: true, autoFocus: true }}
          placeholder={placeholder}
        />
      </Grid>
    </Grid>
  );
};

Input.propTypes = {
  onBackClick: PropTypes.func.isRequired,
  params: PropTypes.shape({
    InputProps: PropTypes.shape({})
  }).isRequired
};

export default Input;
