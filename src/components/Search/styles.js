import { makeStyles } from '@material-ui/core/styles';

export const useAutocompleteStyles = makeStyles(theme => ({
  inputRoot: {
    color: theme.palette.secondary.main
  },
  popperDisablePortal: {
    left: 0,
    top: '52px',
    width: '100% !important' // override inline style of the popper component
  }
}));

export const useStyles = makeStyles(theme => ({
  cancelIcon: {
    color: theme.palette.secondary.main
  }
}));
