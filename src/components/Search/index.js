import React, { useEffect, useMemo, useReducer, useRef } from 'react';
import PropTypes from 'prop-types';
import { Autocomplete } from '@material-ui/lab';
import CancelIcon from '@material-ui/icons/Cancel';
import throttle from 'lodash/throttle';
import Input from './Input';
import Option from './Option';
import { GOOGLE_API, SEARCH_STATUS } from '../../constants';
import createScriptTag from '../../utils/createScriptTag';
import { useStyles, useAutocompleteStyles } from './styles';

// USE REDUCER
const searchInitializer = initialState => {
  return {
    status: initialState,
    value: null,
    inputValue: '',
    options: []
  };
};
const searchReducer = (state, action) => {
  switch (action.type) {
    case SEARCH_STATUS.setOptions: {
      return { ...state, status: SEARCH_STATUS.setOptions, options: action.payload.options };
    }
    case SEARCH_STATUS.setValue: {
      return { ...state, status: SEARCH_STATUS.setValue, value: action.payload.value };
    }
    case SEARCH_STATUS.setInputValue: {
      return { ...state, status: SEARCH_STATUS.setInputValue, inputValue: action.payload.inputValue };
    }
    default: {
      return state;
    }
  }
};

// INITIALIZE AUTOCOMPLETE SERVICE
const autocompleteService = { current: null };

// CREATE SEARCH COMPONENT
const Search = ({ onBackClick }) => {
  const [currentState, dispatch] = useReducer(searchReducer, SEARCH_STATUS.idle, searchInitializer);
  const isGoogleLoaded = useRef(false);
  const customStyles = useStyles();
  const customStylesAutocomplete = useAutocompleteStyles();

  // check if environment is browser and if component is loaded
  if (typeof window !== 'undefined' && !isGoogleLoaded.current) {
    // if no element with #googleMapsApi id is found in the document head, create one.
    if (!document.querySelector('#googleMapsApi')) {
      createScriptTag(GOOGLE_API, document.querySelector('head'), 'googleMapsApi');
    }
    // set loaded reference to true
    isGoogleLoaded.current = true;
  }

  // initialize fetch function for autocomplete service from Google, throttle with lodash
  const fetch = useMemo(
    () =>
      throttle((request, callback) => {
        autocompleteService.current.getPlacePredictions(request, callback);
      }, 200),
    []
  );

  useEffect(() => {
    // mount variable flag
    let active = true;
    // if no autocomplete service instance loaded and no google property present in the global window object create one and add to reference
    if (!autocompleteService.current && window.google) {
      autocompleteService.current = new window.google.maps.places.AutocompleteService();
    }
    // if autocomplete service is false return undefined
    if (!autocompleteService.current) {
      return undefined;
    }
    // if input value is not null add value to options array else return empty
    if (currentState.inputValue === '') {
      if (currentState.value) {
        dispatch({ type: SEARCH_STATUS.setOptions, payload: { options: [currentState.value] } });
      } else {
        dispatch({ type: SEARCH_STATUS.setOptions, payload: { options: [] } });
      }
      return undefined;
    }
    // start fetching, pass arguments to google request parameter, pass inputValue to google's input parameter, restrict types of return to cities only
    fetch({ input: currentState.inputValue, types: ['(cities)'] }, results => {
      // check if component is mounted before executing, to avoid useEffect on a unmounted component
      if (active) {
        // create new newOptions array
        let newOptions = [];
        // if value present add value to options array
        if (currentState.value) {
          newOptions = [currentState.value];
        }
        // if callback from google returns results add options to the newOptions array
        if (results) {
          newOptions = [...newOptions, ...results];
        }
        // set returned options to options array
        dispatch({ type: SEARCH_STATUS.setOptions, payload: { options: newOptions } });
      }
    });
    // set active variable flag to false => component unmounted
    return () => {
      active = false;
    };
  }, [fetch, currentState.value, currentState.inputValue]);

  return (
    <Autocomplete
      autoComplete
      disablePortal
      filterSelectedOptions
      freeSolo
      fullWidth
      includeInputInList
      classes={customStylesAutocomplete}
      closeIcon={<CancelIcon classes={{ root: customStyles.cancelIcon }} />}
      filterOptions={x => x}
      getOptionLabel={option => (typeof option === 'string' ? option : option.description)}
      id='SearchAutocomplete'
      noOptionsText='No Options'
      options={currentState.options}
      renderInput={params => <Input params={params} onBackClick={onBackClick} />}
      renderOption={option => <Option option={option} />}
      value={currentState.value}
      onChange={(event, newValue) => {
        if (newValue) {
          dispatch({ type: SEARCH_STATUS.setOptions, payload: { options: [newValue, ...currentState.options] } });
        } else {
          dispatch({ type: SEARCH_STATUS.setOptions, payload: { options: currentState.options } });
        }
        dispatch({ type: SEARCH_STATUS.setValue, payload: { value: newValue } });
      }}
      onClose={() => onBackClick(false)}
      onInputChange={(event, newInputValue) =>
        dispatch({ type: SEARCH_STATUS.setInputValue, payload: { inputValue: newInputValue } })
      }
    />
  );
};

Search.propTypes = {
  onBackClick: PropTypes.func.isRequired
};

export default Search;
