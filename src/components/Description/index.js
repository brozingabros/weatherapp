import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';

const Description = ({ children }) => (
  <Typography color='primary' component='p' variant='body1'>
    {children}
  </Typography>
);

Description.propTypes = {
  children: PropTypes.string.isRequired
};

export default Description;
