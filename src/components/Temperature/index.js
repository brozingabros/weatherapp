import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import useStyles from './styles';

const Temperature = ({ children, showFaded, showUnit, size }) => {
  const customStyles = useStyles();

  const sizeHandler = value => {
    switch (value) {
      case 'large':
        return 'h1';
      case 'small':
        return 'body1';
      default:
        return 'body1';
    }
  };

  return (
    <Typography
      className={showFaded ? customStyles.temperature.main : customStyles.temperature.light}
      component='p'
      variant={sizeHandler(size)}>
      {showUnit ? `${children}°` : children}
    </Typography>
  );
};

Temperature.propTypes = {
  children: PropTypes.number.isRequired,
  showFaded: PropTypes.bool,
  showUnit: PropTypes.bool,
  size: PropTypes.oneOf(['small', 'large']).isRequired
};

Temperature.defaultProps = {
  showFaded: false,
  showUnit: false
};

export default Temperature;
