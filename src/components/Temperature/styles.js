import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  temperature: {
    main: theme.palette.primary.main,
    light: theme.palette.primary.light
  }
}));

export default useStyles;
