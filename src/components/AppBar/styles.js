import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  bar: {
    marginBottom: '1rem'
  },
  title: {
    flexGrow: 1
  },
  icon: {
    padding: 0
  }
}));

export default useStyles;
