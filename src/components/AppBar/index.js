import React, { useState } from 'react';
import { AppBar, IconButton, Toolbar, Typography } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import Search from '../Search';
import useStyles from './styles';

const Bar = () => {
  const [searchInput, setSearchInput] = useState(false);
  const customStyles = useStyles();

  const handleBackClick = state => setSearchInput(state);

  return (
    <>
      <AppBar className={customStyles.bar} position='static'>
        <Toolbar>
          {searchInput ? (
            <Search onBackClick={handleBackClick} />
          ) : (
            <>
              <Typography className={customStyles.title} component='h1' variant='h6'>
                Weather
              </Typography>
              <IconButton
                aria-label='search'
                classes={{ root: customStyles.icon }}
                color='inherit'
                onClick={() => setSearchInput(true)}>
                <SearchIcon />
              </IconButton>
            </>
          )}
        </Toolbar>
      </AppBar>
    </>
  );
};

export default Bar;
