import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import convertTimestampToDate from '../../utils/convertTimestampToDate';
import useStyles from './styles';

const LastUpdate = ({ children }) => {
  const customStyles = useStyles();
  return (
    <Typography classes={{ root: customStyles.text }} variant='body2'>
      Last update: {convertTimestampToDate(children)}
    </Typography>
  );
};

LastUpdate.propTypes = {
  children: PropTypes.number.isRequired
};

export default LastUpdate;
