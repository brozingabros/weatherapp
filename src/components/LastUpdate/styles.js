import makeStyles from '@material-ui/core/styles/makeStyles';

const useStyles = makeStyles(theme => ({
  text: {
    color: theme.palette.primary.light,
    marginTop: '.2rem'
  }
}));

export default useStyles;
