import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';

const Location = ({ children }) => (
  <Typography color='primary' component='p' variant='h4'>
    {children}
  </Typography>
);

Location.propTypes = {
  children: PropTypes.arrayOf(PropTypes.string).isRequired
};

export default Location;
