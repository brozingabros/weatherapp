import React from 'react';
import PropTypes from 'prop-types';
import { Box } from '@material-ui/core';
import theme from '../../theme';

const TemperatureUnit = ({ color, children, component, fontSize, fontWeight, onClick }) => {
  return (
    <Box color={color} component={component} fontSize={fontSize} fontWeight={fontWeight} onClick={onClick}>
      {children}
    </Box>
  );
};

TemperatureUnit.defaultProps = {
  color: theme.palette.primary.main,
  component: 'span',
  fontSize: '2rem',
  fontWeight: 400,
  onClick: () => null
};

TemperatureUnit.propTypes = {
  color: PropTypes.string,
  children: PropTypes.string.isRequired,
  component: PropTypes.string,
  fontSize: PropTypes.string,
  fontWeight: PropTypes.number,
  onClick: PropTypes.func
};

export default TemperatureUnit;
