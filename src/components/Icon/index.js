import React from 'react';
import PropTypes from 'prop-types';
import { SvgIcon } from '@material-ui/core';
import WEATHER_ICONS from '../../constants/icons';

const Icon = ({ children }) => {
  const WeatherIcon = () => {
    const { component } = WEATHER_ICONS.find(value => value.id.includes(children));
    return component;
  };

  return (
    <SvgIcon aria-label='Current Weather Icon' style={{ fontSize: 120 }} titleAccess='meaning'>
      <WeatherIcon />
    </SvgIcon>
  );
};

Icon.propTypes = {
  children: PropTypes.number.isRequired
};

export default Icon;
