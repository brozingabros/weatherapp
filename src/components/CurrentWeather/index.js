import React, { useContext } from 'react';
import { Box, Grid } from '@material-ui/core';
import { FETCH_STATUS } from '../../constants';
import { DataContext } from '../../context';
import Location from '../Location';
import Description from '../Description';
import Icon from '../Icon';
import Skeleton from './Skeleton';
import TemperatureConverter from '../TemperatureConverter';
import LastUpdate from '../LastUpdate';

const CurrentWeather = () => {
  const [response] = useContext(DataContext);
  const isLoaded = response && response.status === FETCH_STATUS.fetched;

  return (
    <Grid container alignItems='center' component='section' direction='column' justify='center'>
      {isLoaded ? (
        <>
          <Box
            alignItems='center'
            component='section'
            display='flex'
            flexDirection='column'
            mb='3rem'
            px='1rem'
            textAlign='center'>
            <Location>
              {response.data.city.name}, {response.data.city.country}
            </Location>
            <Description>{response.data.list[0].weather[0].description}</Description>
            <LastUpdate>{response.data.list[0].dt}</LastUpdate>
          </Box>
          <Box alignItems='center' component='section' display='flex' flexDirection='column' mb='1rem'>
            <Icon>{response.data.list[0].weather[0].id}</Icon>
          </Box>
          <TemperatureConverter>{response.data.list[0].main.temp}</TemperatureConverter>
        </>
      ) : (
        <Skeleton />
      )}
    </Grid>
  );
};

export default CurrentWeather;
