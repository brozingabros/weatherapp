import React from 'react';
import { Skeleton } from '@material-ui/lab';
import Temperature from '../Temperature';
import Location from '../Location';
import Description from '../Description';
import Icon from '../Icon';

const CurrentWeatherSkeleton = () => {
  return (
    <>
      <Skeleton animation='wave' variant='text'>
        <Location>{['Warsaw', 'Poland']}</Location>
      </Skeleton>
      <Skeleton animation='wave' variant='text'>
        <Description>Clear</Description>
      </Skeleton>
      <Skeleton animation='wave' variant='rect'>
        <Icon>{800}</Icon>
      </Skeleton>
      <Skeleton animation='wave' variant='text'>
        <Temperature size='large'>{0}</Temperature>
      </Skeleton>
    </>
  );
};

export default CurrentWeatherSkeleton;
