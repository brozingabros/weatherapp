import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom/extend-expect';
import App from './App';
import { UrlProvider } from './context';

const AppWithContext = () => {
  return (
    <UrlProvider>
      <App />
    </UrlProvider>
  );
};

describe('App', () => {
  test('renders location name', async () => {
    render(<AppWithContext />);
    expect(await screen.findByText(/warsaw, pl/i)).toBeVisible();
  });
  test('renders weather description', async () => {
    render(<AppWithContext />);
    expect(await screen.findByText(/clouds/i)).toBeVisible();
  });
  test('renders current temperature', async () => {
    render(<AppWithContext />);
    expect(await screen.findByText(/12/i)).toBeVisible();
  });
  test('renders weather icon', async () => {
    render(<AppWithContext />);
    expect(await screen.getByRole('img', { name: /current weather icon/i })).toBeVisible();
  });
  test('renders celsius button', async () => {
    render(<AppWithContext />);
    expect(await screen.findByRole('button', { name: /°C/i })).toBeVisible();
  });
  test('renders fahrenheit button', async () => {
    render(<AppWithContext />);
    expect(await screen.findByRole('button', { name: /°F/i })).toBeVisible();
  });

  test('renders forecast', async () => {
    render(<AppWithContext />);
    expect(await screen.findByText(/thu/i)).toBeVisible();
    expect(await screen.findByText(/fri/i)).toBeVisible();
    expect(await screen.findByText(/sat/i)).toBeVisible();
    expect(await screen.findByText(/8°/i)).toBeVisible();
    expect(await screen.findByText(/9°/i)).toBeVisible();
    expect(await screen.findByText(/10°/i)).toBeVisible();
  });

  test('renders AppBar', () => {
    render(<AppWithContext />);
    expect(screen.getByRole('banner')).toBeVisible();
    expect(screen.getByRole('heading', { name: /weather/i })).toBeVisible();
    expect(screen.getByRole('button', { name: /search/i })).toBeVisible();
  });

  test('renders search', () => {
    render(<AppWithContext />);
    userEvent.click(screen.getByRole('button', { name: /search/i }));
    expect(screen.getByRole('banner')).toBeVisible();
    expect(screen.getByRole('button', { name: /search back button/i })).toBeVisible();
    expect(screen.getByRole('textbox')).toBeVisible();
    expect(screen.getByPlaceholderText(/search location/i)).toBeVisible();
  });

  test('renders last update', async () => {
    render(<AppWithContext />);
    expect(await screen.getByText(/last update: 18:00/i)).toBeVisible();
  });
});

describe('TemperatureConverter', () => {
  test('converts current temperature', async () => {
    render(<AppWithContext />);
    expect(await screen.findByText(/12/i)).toBeVisible();
    expect(await screen.findByRole('button', { name: /°f/i })).toBeVisible();
    userEvent.click(screen.getByRole('button', { name: /°f/i }));
    expect(screen.getByText(/54/i)).toBeVisible();
    userEvent.click(screen.getByRole('button', { name: /°c/i }));
    expect(screen.getByText(/12/i)).toBeVisible();
  });
});
