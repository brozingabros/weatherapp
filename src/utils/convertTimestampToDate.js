const convertTimestampToDate = timestamp => {
  const date = new Date(timestamp * 1000);
  return date.toLocaleString(undefined, {
    hour12: false,
    timeStyle: 'short',
    timeZone: 'UTC'
  });
};

export default convertTimestampToDate;
