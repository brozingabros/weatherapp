import { useEffect, useState } from 'react';

const useCurrentLocation = () => {
  const [error, setError] = useState('');
  const [location, setLocation] = useState(false);

  const handleSuccess = position => {
    const { latitude, longitude } = position.coords;
    setLocation({ latitude, longitude });
  };

  const handleError = err => setError(err.message);

  useEffect(() => {
    if (!navigator.geolocation) {
      setError('Geolocation is not supported');
    }
    navigator.geolocation.getCurrentPosition(handleSuccess, handleError, { enableHighAccuracy: true });
  }, []);

  return { location, error };
};

export default useCurrentLocation;
