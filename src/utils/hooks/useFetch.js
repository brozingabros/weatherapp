import { useCallback, useEffect, useReducer } from 'react';
import useLocalStorage from './useLocalStorage';
import getCurrentHour from '../getCurrentHour';
import { FETCH_STATUS } from '../../constants';

const fetchInitializer = initialState => {
  return {
    status: initialState,
    error: {
      message: '',
      state: false
    },
    data: {}
  };
};

const fetchReducer = (state, action) => {
  switch (action.type) {
    case FETCH_STATUS.fetching:
      return { ...state, status: FETCH_STATUS.fetching };
    case FETCH_STATUS.fetched:
      return { ...state, status: FETCH_STATUS.fetched, data: action.payload.data };
    case FETCH_STATUS.error:
      return {
        ...state,
        status: FETCH_STATUS.error,
        error: { state: action.payload.errorState, message: action.payload.errorMessage }
      };
    default:
      return state;
  }
};

const useFetch = url => {
  const [currentState, dispatch] = useReducer(fetchReducer, FETCH_STATUS.idle, fetchInitializer);
  const { value, setValue } = useLocalStorage('weatherDataCache');

  const handleFetchResponse = response => {
    if (!response.ok || response.status !== 200) {
      return dispatch({
        type: FETCH_STATUS.error,
        payload: { errorMessage: 'Something went wrong with getting the weather data', errorState: true }
      });
    }
    return response.json();
  };

  const handleDataResponse = useCallback(
    data => {
      if (data.success === false) {
        dispatch({ type: FETCH_STATUS.error, payload: { errorState: true, errorMessage: data.error.info } });
      } else {
        const lastFetch = getCurrentHour();
        const dataResponse = { ...data, url, lastFetch };
        setValue(dataResponse);
        dispatch({ type: FETCH_STATUS.fetched, payload: { data } });
      }
    },
    [setValue, url]
  );

  const fetchData = useCallback(() => {
    dispatch({ type: FETCH_STATUS.fetching });
    const currentTime = getCurrentHour();

    if (value && value.url === url && value.lastFetch === currentTime) {
      return dispatch({ type: FETCH_STATUS.fetched, payload: { data: value } });
    }
    return fetch(url).then(handleFetchResponse).then(handleDataResponse).catch(handleFetchResponse);
  }, [handleDataResponse, url, value]);

  useEffect(() => {
    if (!url) return;
    fetchData();
  }, [url, fetchData]);

  return { response: currentState };
};

export default useFetch;
