import { useCallback, useState } from 'react';

const useLocalStorage = (key, initialValue) => {
  const [value, setStoredValue] = useState(() => {
    try {
      const item = window.localStorage.getItem(key);
      return item ? JSON.parse(item) : initialValue;
    } catch (error) {
      return initialValue;
    }
  });
  const setValue = useCallback(
    val => {
      try {
        const valueToStore = val instanceof Function ? val(val) : val;
        setStoredValue(valueToStore);
        window.localStorage.setItem(key, JSON.stringify(valueToStore));
      } catch (error) {
        // error;
      }
    },
    [key]
  );

  return { value, setValue };
};

export default useLocalStorage;
