const getCurrentHour = () => {
  const currentTimestamp = new Date();
  return currentTimestamp.getHours();
};

export default getCurrentHour;
