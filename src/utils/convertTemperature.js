const convertCtoF = value => value * (9 / 5) + 32;
const convertFtoC = value => ((value - 32) * 5) / 9;

export { convertCtoF, convertFtoC };
