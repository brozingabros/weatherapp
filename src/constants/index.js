export const FETCH_STATUS = {
  idle: 'IDLE',
  fetching: 'FETCHING',
  fetched: 'FETCHED',
  error: 'ERROR'
};

export const SEARCH_STATUS = {
  idle: 'IDLE',
  setInputValue: 'SET_INPUT_VALUE',
  setValue: 'SET_VALUE',
  setOptions: 'SET_OPTIONS'
};

export const API = {
  url: 'https://api.openweathermap.org/data/2.5/forecast',
  key: 'appid=5cd952fa73b6d96bf2a5f9c09ff98178',
  city_id: 'q=Antwerp,be',
  units: 'units=metric'
};

export const GOOGLE_API =
  'https://maps.googleapis.com/maps/api/js?key=AIzaSyDQ7WkfEatcLxfcb2fC4dfp9HcQiHwWGSs&libraries=places';

export const TEMP_UNITS = {
  celsius: '°C',
  fahrenheit: '°F'
};
