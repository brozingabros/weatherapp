import React from 'react';

const Thunderstorm = () => {
  return (
    <svg fill='none' viewBox='0 0 90 79' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M30.8 47.06l-7.022 12.162c-1.1 1.907-.204 3.334 1.99 3.172l5.735-.421c2.195-.163 3.13 1.287 2.08 3.221l-6.38 11.742c-1.05 1.933-.52 2.372 1.179.973l23.07-18.97c1.698-1.4 1.29-2.45-.907-2.34l-8.814.454c-2.197.11-3.096-1.356-1.994-3.261l3.878-6.719c1.1-1.907.2-3.467-2-3.47l-4.816-.002c-2.198-.003-4.898 1.553-6 3.459'
        fill='url(#paint0_linear)'
      />
      <path
        d='M61.337 48.584c7.852 0 14.219-6.365 14.219-14.217 0-7.854-6.367-14.219-14.219-14.219-.049 0-.097.007-.145.007C59.927 8.816 50.315 0 38.639 0c-9.222 0-17.152 5.5-20.704 13.397-.114-.002-.224-.008-.338-.008C7.88 13.39 0 21.268 0 30.987c0 9.72 7.879 17.597 17.597 17.597'
        fill='#F2F2F2'
      />
      <path
        d='M53.04 26.49c-.071-.001-.14-.006-.211-.006-6.104 0-11.05 4.949-11.05 11.05 0 6.103 4.946 11.05 11.05 11.05h27.465a8.928 8.928 0 000-17.857c-.03 0-.061.006-.09.006-.795-7.12-6.831-12.656-14.163-12.656-5.79 0-10.77 3.454-13.001 8.413'
        fill='url(#paint1_linear)'
      />
      <defs>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint0_linear'
          x1='21.052'
          x2='46.662'
          y1='72.963'
          y2='47.353'>
          <stop stopColor='#FDB727' />
          <stop offset='1' stopColor='#FFCE22' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint1_linear'
          x1='51.661'
          x2='78.761'
          y1='51.995'
          y2='24.895'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
      </defs>
    </svg>
  );
};
const ThunderstormRain = () => {
  return (
    <svg fill='none' viewBox='0 0 90 79' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M48.134 47.06l-7.022 12.162c-1.102 1.906-.206 3.333 1.989 3.17l5.736-.42c2.193-.162 3.13 1.287 2.08 3.22l-6.381 11.742c-1.05 1.933-.519 2.372 1.18.973l23.069-18.972c1.699-1.396 1.292-2.45-.907-2.336l-8.813.45c-2.197.114-3.096-1.354-1.995-3.258l3.88-6.72c1.1-1.905.2-3.465-2-3.468l-4.816-.005c-2.2-.002-4.9 1.557-6 3.461'
        fill='url(#paint0_linear)'
      />
      <path
        d='M61.337 48.584c7.854 0 14.219-6.365 14.219-14.219 0-7.852-6.365-14.218-14.219-14.218-.049 0-.096.008-.145.008C59.927 8.816 50.315 0 38.639 0c-9.22 0-17.152 5.5-20.704 13.397-.112-.002-.224-.008-.336-.008C7.879 13.39 0 21.268 0 30.987c0 9.718 7.879 17.597 17.599 17.597'
        fill='#F2F2F2'
      />
      <path
        d='M53.04 26.489c-.071-.001-.142-.004-.212-.004-6.103 0-11.05 4.945-11.05 11.05 0 6.102 4.947 11.05 11.05 11.05h27.466a8.93 8.93 0 000-17.857c-.03 0-.061.005-.09.005-.795-7.12-6.833-12.657-14.163-12.657-5.79 0-10.77 3.454-13.002 8.413'
        fill='url(#paint1_linear)'
      />
      <path
        d='M14.11 59.596L5.445 74.605a2.666 2.666 0 104.619 2.665l8.665-15.008a2.667 2.667 0 00-4.619-2.666z'
        fill='url(#paint2_linear)'
      />
      <path
        d='M28.289 59.596l-8.665 15.009a2.667 2.667 0 104.618 2.665l8.666-15.008a2.667 2.667 0 00-4.62-2.666z'
        fill='url(#paint3_linear)'
      />
      <defs>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint0_linear'
          x1='38.386'
          x2='63.996'
          y1='72.962'
          y2='47.352'>
          <stop stopColor='#FDB727' />
          <stop offset='1' stopColor='#FFCE22' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint1_linear'
          x1='51.661'
          x2='78.761'
          y1='51.995'
          y2='24.895'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint2_linear'
          x1='5.087'
          x2='19.086'
          y1='68.433'
          y2='68.433'>
          <stop stopColor='#00AEEF' />
          <stop offset='1' stopColor='#00BDF2' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint3_linear'
          x1='19.266'
          x2='33.265'
          y1='68.433'
          y2='68.433'>
          <stop stopColor='#00AEEF' />
          <stop offset='1' stopColor='#00BDF2' />
        </linearGradient>
      </defs>
    </svg>
  );
};
const ThunderStormDrizzle = () => {
  return (
    <svg fill='none' viewBox='0 0 90 79' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M48.134 47.06l-7.022 12.162c-1.102 1.906-.206 3.333 1.989 3.17l5.736-.42c2.193-.162 3.13 1.287 2.08 3.22l-6.381 11.742c-1.05 1.933-.519 2.372 1.18.973l23.069-18.972c1.699-1.396 1.292-2.45-.907-2.336l-8.813.45c-2.197.114-3.096-1.354-1.995-3.258l3.88-6.72c1.1-1.905.2-3.465-2-3.468l-4.816-.005c-2.2-.002-4.9 1.557-6 3.461'
        fill='url(#paint0_linear)'
      />
      <path
        d='M61.337 48.584c7.854 0 14.219-6.365 14.219-14.219 0-7.852-6.365-14.218-14.219-14.218-.049 0-.096.008-.145.008C59.927 8.816 50.315 0 38.639 0c-9.22 0-17.152 5.5-20.704 13.397-.112-.002-.224-.008-.336-.008C7.879 13.39 0 21.268 0 30.987c0 9.718 7.879 17.597 17.599 17.597'
        fill='#F2F2F2'
      />
      <path
        d='M53.04 26.489c-.071-.001-.142-.004-.212-.004-6.103 0-11.05 4.945-11.05 11.05 0 6.102 4.947 11.05 11.05 11.05h27.466a8.93 8.93 0 000-17.857c-.03 0-.061.005-.09.005-.795-7.12-6.833-12.657-14.163-12.657-5.79 0-10.77 3.454-13.002 8.413'
        fill='url(#paint1_linear)'
      />
      <path
        d='M15.207 59.336L11.357 66a2.668 2.668 0 004.62 2.667L19.827 62A2.668 2.668 0 0017.518 58c-.923 0-1.817.48-2.31 1.336'
        fill='url(#paint2_linear)'
      />
      <path
        d='M28.206 59.335l-3.848 6.667a2.667 2.667 0 104.62 2.666l3.848-6.666a2.665 2.665 0 00-.978-3.643 2.665 2.665 0 00-3.643.976z'
        fill='url(#paint3_linear)'
      />
      <defs>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint0_linear'
          x1='38.386'
          x2='63.996'
          y1='72.962'
          y2='47.352'>
          <stop stopColor='#FDB727' />
          <stop offset='1' stopColor='#FFCE22' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint1_linear'
          x1='51.661'
          x2='78.761'
          y1='51.995'
          y2='24.895'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint2_linear'
          x1='11.001'
          x2='20.184'
          y1='64.001'
          y2='64.001'>
          <stop stopColor='#00AEEF' />
          <stop offset='1' stopColor='#00BDF2' />
        </linearGradient>
        <linearGradient gradientUnits='userSpaceOnUse' id='paint3_linear' x1='24' x2='33.184' y1='64.001' y2='64.001'>
          <stop stopColor='#00AEEF' />
          <stop offset='1' stopColor='#00BDF2' />
        </linearGradient>
      </defs>
    </svg>
  );
};
const Clear = () => {
  return (
    <svg fill='none' viewBox='0 0 56 55' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M.868 27.312c0 15.061 12.21 27.272 27.273 27.272 15.063 0 27.274-12.21 27.274-27.272C55.415 12.248 43.204.037 28.14.037 13.077.037.868 12.248.868 27.312z'
        fill='url(#paint0_linear)'
      />
      <defs>
        <linearGradient gradientUnits='userSpaceOnUse' id='paint0_linear' x1='8.856' x2='47.426' y1='46.596' y2='8.026'>
          <stop stopColor='#FDB727' />
          <stop offset='1' stopColor='#FFCE22' />
        </linearGradient>
      </defs>
    </svg>
  );
};
const FewClouds = () => {
  return (
    <svg fill='none' viewBox='0 0 90 55' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M47.587 17.977c0 9.547 7.737 17.284 17.282 17.284 9.547 0 17.286-7.737 17.286-17.284 0-9.545-7.739-17.284-17.286-17.284-9.545 0-17.282 7.739-17.282 17.284z'
        fill='url(#paint0_linear)'
      />
      <path
        d='M61.777 54.373c7.852 0 14.219-6.366 14.219-14.22 0-7.851-6.367-14.216-14.22-14.216-.049 0-.097.006-.145.006C60.367 14.605 50.755 5.79 39.079 5.79c-9.221 0-17.152 5.5-20.705 13.397-.112-.003-.223-.008-.336-.008-9.719 0-17.598 7.879-17.598 17.597 0 9.719 7.88 17.598 17.598 17.598'
        fill='#F2F2F2'
      />
      <path
        d='M53.478 32.277c-.069-.001-.14-.005-.212-.005-6.1 0-11.05 4.946-11.05 11.05 0 6.104 4.95 11.051 11.05 11.051h27.467a8.928 8.928 0 008.928-8.93 8.928 8.928 0 00-8.928-8.927c-.03 0-.06.004-.092.005-.794-7.12-6.83-12.657-14.161-12.657-5.79 0-10.77 3.456-13.002 8.413z'
        fill='url(#paint1_linear)'
      />
      <defs>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint0_linear'
          x1='52.649'
          x2='77.092'
          y1='30.199'
          y2='5.756'>
          <stop stopColor='#FDB727' />
          <stop offset='1' stopColor='#FFCE22' />
        </linearGradient>
        <linearGradient gradientUnits='userSpaceOnUse' id='paint1_linear' x1='52.1' x2='79.2' y1='57.783' y2='30.683'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
      </defs>
    </svg>
  );
};
const ScatteredClouds = () => {
  return (
    <svg fill='none' viewBox='0 0 93 53' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M36.153 26.113c0 9.982-8.093 18.075-18.077 18.075C8.093 44.188 0 36.096 0 26.112S8.093 8.035 18.076 8.035c9.984 0 18.077 8.093 18.077 18.078z'
        fill='#D0E8F8'
      />
      <path
        d='M27.315 36.213c0-9.136 7.407-16.542 16.543-16.542s16.54 7.406 16.54 16.542c0 9.137-7.404 16.542-16.54 16.542S27.315 45.35 27.315 36.213z'
        fill='#F2F2F2'
      />
      <path
        d='M78.62 18.446c0 7.744-6.276 14.022-14.022 14.022-7.742 0-14.021-6.278-14.021-14.022 0-7.744 6.279-14.023 14.021-14.023 7.746 0 14.023 6.279 14.023 14.023z'
        fill='#F2F2F2'
      />
      <path
        d='M53.721 13.523c0 7.468-6.054 13.522-13.522 13.522s-13.522-6.054-13.522-13.522C26.677 6.053 32.731 0 40.2 0S53.72 6.053 53.72 13.523z'
        fill='#D0E8F8'
      />
      <path
        d='M69.138 28.241c0 6.528 5.292 11.82 11.82 11.82 6.528 0 11.82-5.292 11.82-11.82 0-6.528-5.292-11.82-11.82-11.82-6.528 0-11.82 5.292-11.82 11.82z'
        fill='#F2F2F2'
      />
      <path
        d='M54.713 31.142c0-6.528 5.292-11.82 11.82-11.82 6.527 0 11.819 5.292 11.819 11.82 0 6.528-5.292 11.82-11.819 11.82-6.528 0-11.82-5.292-11.82-11.82z'
        fill='#D0E8F8'
      />
    </svg>
  );
};
const BrokenClouds = () => {
  return (
    <svg fill='none' viewBox='0 0 90 49' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M61.337 48.584c7.852 0 14.219-6.365 14.219-14.219 0-7.852-6.367-14.218-14.219-14.218-.049 0-.096.008-.145.008C59.927 8.816 50.315 0 38.639 0c-9.22 0-17.152 5.5-20.704 13.397-.114-.002-.224-.008-.338-.008C7.88 13.39 0 21.268 0 30.987c0 9.718 7.879 17.597 17.597 17.597'
        fill='#F2F2F2'
      />
      <path
        d='M53.04 26.488c-.071 0-.14-.003-.213-.003-6.102 0-11.049 4.946-11.049 11.049 0 6.102 4.947 11.05 11.05 11.05h27.466a8.929 8.929 0 008.928-8.929 8.928 8.928 0 00-8.928-8.928c-.03 0-.06.005-.092.005-.795-7.12-6.83-12.657-14.163-12.657-5.788 0-10.769 3.455-13 8.413z'
        fill='url(#paint0_linear)'
      />
      <defs>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint0_linear'
          x1='51.661'
          x2='78.761'
          y1='51.994'
          y2='24.894'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
      </defs>
    </svg>
  );
};
const Rain = () => {
  return (
    <svg fill='none' viewBox='0 0 90 79' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M61.337 48.585c7.854 0 14.219-6.365 14.219-14.218 0-7.852-6.365-14.219-14.219-14.219-.049 0-.096.007-.145.008C59.927 8.817 50.315 0 38.639 0 29.419 0 21.487 5.501 17.935 13.4c-.112-.003-.224-.008-.336-.008C7.879 13.39 0 21.27 0 30.988c0 9.719 7.879 17.597 17.599 17.597'
        fill='#F2F2F2'
      />
      <path
        d='M53.04 26.491c-.071-.001-.142-.006-.212-.006-6.103 0-11.05 4.948-11.05 11.05 0 6.104 4.947 11.05 11.05 11.05h27.466a8.928 8.928 0 000-17.857c-.03 0-.061.006-.09.006-.795-7.122-6.833-12.656-14.163-12.656-5.79 0-10.77 3.453-13.002 8.413'
        fill='url(#paint0_linear)'
      />
      <path
        d='M14.11 59.715L5.445 74.723a2.667 2.667 0 104.619 2.667l8.665-15.008a2.67 2.67 0 00-2.307-4.001c-.921 0-1.818.478-2.312 1.334z'
        fill='url(#paint1_linear)'
      />
      <path
        d='M28.956 59.715L20.29 74.723a2.67 2.67 0 002.307 4.002c.923 0 1.819-.48 2.312-1.335l8.665-15.008a2.668 2.668 0 00-2.306-4.001c-.922 0-1.82.478-2.312 1.334z'
        fill='url(#paint2_linear)'
      />
      <path
        d='M43.8 59.715l-8.663 15.008a2.668 2.668 0 104.619 2.667l8.665-15.008a2.67 2.67 0 00-2.308-4.001c-.921 0-1.818.478-2.312 1.334z'
        fill='url(#paint3_linear)'
      />
      <defs>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint0_linear'
          x1='51.661'
          x2='78.761'
          y1='51.996'
          y2='24.896'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint1_linear'
          x1='5.087'
          x2='19.086'
          y1='68.552'
          y2='68.552'>
          <stop stopColor='#00AEEF' />
          <stop offset='1' stopColor='#00BDF2' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint2_linear'
          x1='19.933'
          x2='33.932'
          y1='68.552'
          y2='68.552'>
          <stop stopColor='#00AEEF' />
          <stop offset='1' stopColor='#00BDF2' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint3_linear'
          x1='34.779'
          x2='48.778'
          y1='68.552'
          y2='68.552'>
          <stop stopColor='#00AEEF' />
          <stop offset='1' stopColor='#00BDF2' />
        </linearGradient>
      </defs>
    </svg>
  );
};
const ShowerRain = () => {
  return (
    <svg fill='none' viewBox='0 0 90 80' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M61.337 48.584c7.852 0 14.219-6.365 14.219-14.219 0-7.852-6.367-14.218-14.219-14.218-.049 0-.096.008-.145.008C59.927 8.816 50.315 0 38.639 0c-9.22 0-17.152 5.5-20.704 13.397-.114-.002-.224-.008-.338-.008C7.88 13.39 0 21.268 0 30.987c0 9.718 7.879 17.597 17.597 17.597'
        fill='#F2F2F2'
      />
      <path
        d='M53.038 26.488c-.07 0-.139-.004-.21-.004-6.102 0-11.05 4.947-11.05 11.05 0 6.102 4.948 11.05 11.05 11.05h27.466a8.93 8.93 0 000-17.857c-.03 0-.061.005-.09.005-.795-7.12-6.831-12.656-14.163-12.656-5.79 0-10.771 3.454-13.003 8.412z'
        fill='url(#paint0_linear)'
      />
      <path
        d='M14.398 59.715l-3.85 6.665a2.668 2.668 0 004.62 2.667l3.85-6.667a2.668 2.668 0 00-2.31-4.001c-.922 0-1.817.48-2.31 1.336'
        fill='url(#paint1_linear)'
      />
      <path
        d='M35.88 59.715l-3.85 6.665a2.667 2.667 0 004.62 2.667l3.85-6.667a2.668 2.668 0 00-4.62-2.665'
        fill='url(#paint2_linear)'
      />
      <path
        d='M57.362 59.715l-3.85 6.665a2.667 2.667 0 004.62 2.667l3.85-6.667a2.668 2.668 0 00-4.62-2.665'
        fill='url(#paint3_linear)'
      />
      <path
        d='M21.063 69.287l-3.848 6.666a2.666 2.666 0 104.62 2.667l3.848-6.667a2.666 2.666 0 00-.977-3.642 2.665 2.665 0 00-3.643.976z'
        fill='url(#paint4_linear)'
      />
      <path
        d='M43.214 69.287l-3.85 6.666a2.667 2.667 0 004.62 2.667l3.848-6.667a2.667 2.667 0 10-4.618-2.666z'
        fill='url(#paint5_linear)'
      />
      <path
        d='M65.362 69.287l-3.85 6.666a2.668 2.668 0 004.62 2.667l3.85-6.667a2.667 2.667 0 00-4.62-2.666'
        fill='url(#paint6_linear)'
      />
      <defs>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint0_linear'
          x1='51.661'
          x2='78.761'
          y1='51.994'
          y2='24.894'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient gradientUnits='userSpaceOnUse' id='paint1_linear' x1='10.191' x2='19.375' y1='64.38' y2='64.38'>
          <stop stopColor='#00AEEF' />
          <stop offset='1' stopColor='#00BDF2' />
        </linearGradient>
        <linearGradient gradientUnits='userSpaceOnUse' id='paint2_linear' x1='31.674' x2='40.857' y1='64.38' y2='64.38'>
          <stop stopColor='#00AEEF' />
          <stop offset='1' stopColor='#00BDF2' />
        </linearGradient>
        <linearGradient gradientUnits='userSpaceOnUse' id='paint3_linear' x1='53.156' x2='62.34' y1='64.38' y2='64.38'>
          <stop stopColor='#00AEEF' />
          <stop offset='1' stopColor='#00BDF2' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint4_linear'
          x1='16.858'
          x2='26.041'
          y1='73.953'
          y2='73.953'>
          <stop stopColor='#00AEEF' />
          <stop offset='1' stopColor='#00BDF2' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint5_linear'
          x1='39.007'
          x2='48.191'
          y1='73.953'
          y2='73.953'>
          <stop stopColor='#00AEEF' />
          <stop offset='1' stopColor='#00BDF2' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint6_linear'
          x1='61.156'
          x2='70.339'
          y1='73.953'
          y2='73.953'>
          <stop stopColor='#00AEEF' />
          <stop offset='1' stopColor='#00BDF2' />
        </linearGradient>
      </defs>
    </svg>
  );
};
const FreezingRain = () => {
  return (
    <svg fill='none' viewBox='0 0 90 87' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M61.337 48.584c7.852 0 14.219-6.365 14.219-14.219 0-7.852-6.367-14.218-14.219-14.218-.049 0-.097.008-.145.008C59.927 8.816 50.315 0 38.639 0c-9.222 0-17.152 5.5-20.706 13.397-.112-.002-.222-.008-.336-.008C7.88 13.39 0 21.268 0 30.987c0 9.718 7.879 17.597 17.597 17.597'
        fill='#F2F2F2'
      />
      <path
        d='M53.04 26.49c-.07-.002-.14-.006-.213-.006-6.101 0-11.05 4.947-11.05 11.05 0 6.104 4.949 11.049 11.05 11.049h27.467a8.927 8.927 0 008.928-8.927 8.93 8.93 0 00-8.928-8.929c-.031 0-.06.005-.092.005-.795-7.12-6.83-12.656-14.162-12.656-5.79 0-10.77 3.454-13 8.414'
        fill='url(#paint0_linear)'
      />
      <path
        d='M15.443 59.714L6.778 74.722a2.667 2.667 0 104.62 2.666l8.665-15.008a2.668 2.668 0 00-4.62-2.666'
        fill='url(#paint1_linear)'
      />
      <path
        d='M29.63 59.714L16.298 82.807a2.666 2.666 0 104.62 2.667L34.25 62.381a2.668 2.668 0 00-4.621-2.667z'
        fill='url(#paint2_linear)'
      />
      <path d='M44.25 61.046a2.668 2.668 0 105.335-.002 2.668 2.668 0 00-5.335.002z' fill='url(#paint3_linear)' />
      <path d='M57.583 61.046a2.668 2.668 0 105.336-.002 2.668 2.668 0 00-5.336.002z' fill='url(#paint4_linear)' />
      <path d='M32.25 83.19a2.667 2.667 0 105.333-.003 2.667 2.667 0 00-5.333.003z' fill='url(#paint5_linear)' />
      <path d='M38.918 71.855a2.667 2.667 0 105.333.002 2.667 2.667 0 00-5.333-.002z' fill='url(#paint6_linear)' />
      <path d='M52.251 71.855a2.667 2.667 0 105.334.002 2.667 2.667 0 00-5.334-.002z' fill='url(#paint7_linear)' />
      <defs>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint0_linear'
          x1='51.661'
          x2='78.761'
          y1='51.995'
          y2='24.895'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient gradientUnits='userSpaceOnUse' id='paint1_linear' x1='6.42' x2='20.42' y1='68.551' y2='68.551'>
          <stop stopColor='#00AEEF' />
          <stop offset='1' stopColor='#00BDF2' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint2_linear'
          x1='15.939'
          x2='34.607'
          y1='72.594'
          y2='72.594'>
          <stop stopColor='#00AEEF' />
          <stop offset='1' stopColor='#00BDF2' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint3_linear'
          x1='44.251'
          x2='49.584'
          y1='61.045'
          y2='61.045'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint4_linear'
          x1='57.584'
          x2='62.917'
          y1='61.045'
          y2='61.045'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint5_linear'
          x1='32.251'
          x2='37.584'
          y1='83.189'
          y2='83.189'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint6_linear'
          x1='38.917'
          x2='44.251'
          y1='71.855'
          y2='71.855'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint7_linear'
          x1='52.25'
          x2='57.584'
          y1='71.855'
          y2='71.855'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
      </defs>
    </svg>
  );
};
const Drizzle = () => {
  return (
    <svg fill='none' viewBox='0 0 90 72' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M61.337 49.492c7.852 0 14.219-6.365 14.219-14.218 0-7.854-6.367-14.22-14.219-14.22-.049 0-.096.008-.145.009C59.927 9.724 50.315.907 38.639.907c-9.22 0-17.152 5.501-20.704 13.397-.114-.001-.224-.007-.338-.007C7.88 14.297 0 22.177 0 31.895c0 9.719 7.879 17.597 17.597 17.597'
        fill='#F2F2F2'
      />
      <path
        d='M53.04 27.397c-.071-.001-.14-.004-.213-.004-6.102 0-11.049 4.945-11.049 11.05 0 6.1 4.947 11.049 11.05 11.049h27.466a8.928 8.928 0 000-17.856c-.03 0-.06.004-.092.004-.795-7.12-6.83-12.658-14.163-12.658-5.788 0-10.769 3.455-13 8.415'
        fill='url(#paint0_linear)'
      />
      <path
        d='M18.014 60.62l-3.85 6.668a2.667 2.667 0 004.62 2.666l3.85-6.666a2.667 2.667 0 00-4.62-2.667'
        fill='url(#paint1_linear)'
      />
      <path
        d='M36.163 60.62l-3.85 6.668a2.667 2.667 0 004.62 2.666l3.85-6.666a2.667 2.667 0 00-4.62-2.667'
        fill='url(#paint2_linear)'
      />
      <path
        d='M55.646 60.62l-3.85 6.668a2.666 2.666 0 104.62 2.666l3.85-6.666a2.668 2.668 0 00-4.62-2.667'
        fill='url(#paint3_linear)'
      />
      <defs>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint0_linear'
          x1='51.661'
          x2='78.761'
          y1='52.902'
          y2='25.802'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint1_linear'
          x1='13.807'
          x2='22.991'
          y1='65.287'
          y2='65.287'>
          <stop stopColor='#00AEEF' />
          <stop offset='1' stopColor='#00BDF2' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint2_linear'
          x1='31.957'
          x2='41.14'
          y1='65.287'
          y2='65.287'>
          <stop stopColor='#00AEEF' />
          <stop offset='1' stopColor='#00BDF2' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint3_linear'
          x1='51.439'
          x2='60.622'
          y1='65.287'
          y2='65.287'>
          <stop stopColor='#00AEEF' />
          <stop offset='1' stopColor='#00BDF2' />
        </linearGradient>
      </defs>
    </svg>
  );
};
const Snow = () => {
  return (
    <svg fill='none' viewBox='0 0 90 82' xmlns='http://www.w3.org/2000/svg'>
      <path
        d='M61.337 48.585c7.854 0 14.219-6.365 14.219-14.218s-6.365-14.219-14.219-14.219c-.049 0-.096.007-.145.008C59.927 8.817 50.315 0 38.639 0c-9.22 0-17.152 5.501-20.704 13.398-.114-.002-.224-.008-.338-.008C7.88 13.39 0 21.27 0 30.987c0 9.719 7.879 17.597 17.597 17.597'
        fill='#F2F2F2'
      />
      <path
        d='M53.041 26.49c-.072-.002-.141-.007-.213-.007-6.102 0-11.05 4.95-11.05 11.05 0 6.105 4.948 11.051 11.05 11.051h27.466a8.928 8.928 0 000-17.856c-.03 0-.061.005-.092.005-.793-7.12-6.829-12.657-14.161-12.657-5.79 0-10.77 3.455-13 8.414'
        fill='url(#paint0_linear)'
      />
      <path d='M13.704 66.07a6.442 6.442 0 1012.883-.001 6.442 6.442 0 00-12.883 0z' fill='url(#paint1_linear)' />
      <path d='M50.92 66.07a6.441 6.441 0 1012.884 0 6.441 6.441 0 00-12.883 0z' fill='url(#paint2_linear)' />
      <path d='M32.954 75.386a6.441 6.441 0 1012.883 0 6.441 6.441 0 00-12.883 0z' fill='url(#paint3_linear)' />
      <defs>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint0_linear'
          x1='51.661'
          x2='78.762'
          y1='51.995'
          y2='24.895'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient gradientUnits='userSpaceOnUse' id='paint1_linear' x1='13.704' x2='26.586' y1='66.07' y2='66.07'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient gradientUnits='userSpaceOnUse' id='paint2_linear' x1='50.922' x2='63.803' y1='66.07' y2='66.07'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint3_linear'
          x1='32.955'
          x2='45.836'
          y1='75.387'
          y2='75.387'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
      </defs>
    </svg>
  );
};
const ShowerSnow = () => {
  return (
    <svg fill='none' viewBox='0 0 90 87' xmlns='http://www.w3.org/2000/svg'>
      <path d='M14.848 62.297a2.666 2.666 0 105.331 0 2.666 2.666 0 00-5.331 0z' fill='url(#paint0_linear)' />
      <path
        d='M28.18 62.297a2.666 2.666 0 002.667 2.666 2.666 2.666 0 100-5.332 2.666 2.666 0 00-2.666 2.666z'
        fill='url(#paint1_linear)'
      />
      <path d='M41.514 62.297a2.666 2.666 0 105.334-.001 2.666 2.666 0 00-5.334.002z' fill='url(#paint2_linear)' />
      <path
        d='M54.848 62.297a2.666 2.666 0 002.666 2.666 2.666 2.666 0 100-5.332 2.666 2.666 0 00-2.666 2.666z'
        fill='url(#paint3_linear)'
      />
      <path d='M14.848 84.006a2.666 2.666 0 105.332.001 2.666 2.666 0 00-5.332-.001z' fill='url(#paint4_linear)' />
      <path d='M28.18 84.006a2.666 2.666 0 105.333.001 2.666 2.666 0 00-5.332-.001z' fill='url(#paint5_linear)' />
      <path d='M41.514 84.006a2.666 2.666 0 105.333 0 2.666 2.666 0 00-5.333 0z' fill='url(#paint6_linear)' />
      <path d='M54.848 84.006a2.666 2.666 0 105.332.001 2.666 2.666 0 00-5.332-.001z' fill='url(#paint7_linear)' />
      <path d='M8.18 73.339a2.667 2.667 0 105.335 0 2.667 2.667 0 00-5.335 0z' fill='url(#paint8_linear)' />
      <path d='M21.514 73.339a2.667 2.667 0 105.334.002 2.667 2.667 0 00-5.334-.002z' fill='url(#paint9_linear)' />
      <path d='M34.848 73.339a2.667 2.667 0 105.334.002 2.667 2.667 0 00-5.334-.002z' fill='url(#paint10_linear)' />
      <path d='M48.18 73.339a2.667 2.667 0 105.335.002 2.667 2.667 0 00-5.334-.002z' fill='url(#paint11_linear)' />
      <path
        d='M61.337 48.585c7.852 0 14.219-6.365 14.219-14.218s-6.367-14.219-14.219-14.219c-.049 0-.097.007-.145.008C59.927 8.817 50.315 0 38.639 0c-9.222 0-17.152 5.501-20.704 13.397-.114 0-.224-.006-.338-.006C7.88 13.39 0 21.27 0 30.988c0 9.719 7.879 17.597 17.597 17.597'
        fill='#F2F2F2'
      />
      <path
        d='M53.04 26.49c-.071 0-.14-.005-.211-.005-6.104 0-11.05 4.947-11.05 11.05 0 6.103 4.946 11.05 11.05 11.05h27.465a8.93 8.93 0 000-17.857c-.03 0-.061.006-.09.006-.795-7.122-6.831-12.658-14.163-12.658-5.79 0-10.77 3.455-13.001 8.415z'
        fill='url(#paint12_linear)'
      />
      <defs>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint0_linear'
          x1='14.848'
          x2='20.181'
          y1='62.296'
          y2='62.296'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint1_linear'
          x1='28.18'
          x2='33.514'
          y1='62.296'
          y2='62.296'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint2_linear'
          x1='41.514'
          x2='46.847'
          y1='62.296'
          y2='62.296'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint3_linear'
          x1='54.847'
          x2='60.181'
          y1='62.296'
          y2='62.296'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint4_linear'
          x1='14.848'
          x2='20.181'
          y1='84.007'
          y2='84.007'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint5_linear'
          x1='28.18'
          x2='33.514'
          y1='84.007'
          y2='84.007'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint6_linear'
          x1='41.514'
          x2='46.847'
          y1='84.007'
          y2='84.007'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint7_linear'
          x1='54.847'
          x2='60.181'
          y1='84.007'
          y2='84.007'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient gradientUnits='userSpaceOnUse' id='paint8_linear' x1='8.181' x2='13.514' y1='73.34' y2='73.34'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient gradientUnits='userSpaceOnUse' id='paint9_linear' x1='21.514' x2='26.847' y1='73.34' y2='73.34'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint10_linear'
          x1='34.847'
          x2='40.181'
          y1='73.34'
          y2='73.34'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint11_linear'
          x1='48.181'
          x2='53.514'
          y1='73.34'
          y2='73.34'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient
          gradientUnits='userSpaceOnUse'
          id='paint12_linear'
          x1='51.661'
          x2='78.761'
          y1='51.996'
          y2='24.896'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
      </defs>
    </svg>
  );
};
const Atmosphere = () => {
  return (
    <svg fill='none' viewBox='0 0 58 57' xmlns='http://www.w3.org/2000/svg'>
      <path d='M2.667 0a2.666 2.666 0 100 5.333h51.998a2.666 2.666 0 100-5.333H2.667z' fill='url(#paint0_linear)' />
      <path d='M13.333 12.834a2.667 2.667 0 000 5.333h28a2.667 2.667 0 100-5.333h-28z' fill='url(#paint1_linear)' />
      <path d='M21.333 25.667a2.668 2.668 0 100 5.333h24a2.668 2.668 0 000-5.333h-24z' fill='url(#paint2_linear)' />
      <path d='M21.333 38.5a2.667 2.667 0 100 5.332h16a2.667 2.667 0 000-5.332h-16z' fill='url(#paint3_linear)' />
      <path
        d='M23.332 51.332a2.667 2.667 0 000 5.335h1.333a2.667 2.667 0 100-5.335h-1.333z'
        fill='url(#paint4_linear)'
      />
      <defs>
        <linearGradient gradientUnits='userSpaceOnUse' id='paint0_linear' x1='0' x2='57.333' y1='2.667' y2='2.667'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient gradientUnits='userSpaceOnUse' id='paint1_linear' x1='10.667' x2='44' y1='15.5' y2='15.5'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient gradientUnits='userSpaceOnUse' id='paint2_linear' x1='18.666' x2='48' y1='28.333' y2='28.333'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient gradientUnits='userSpaceOnUse' id='paint3_linear' x1='18.666' x2='40' y1='41.167' y2='41.167'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
        <linearGradient gradientUnits='userSpaceOnUse' id='paint4_linear' x1='20.667' x2='27.333' y1='54' y2='54'>
          <stop stopColor='#D0E8F8' />
          <stop offset='1' stopColor='#E2EEF9' />
        </linearGradient>
      </defs>
    </svg>
  );
};

const WEATHER_ICONS = [
  { id: [210, 211, 212, 221], component: <Thunderstorm /> },
  { id: [200, 201, 202], component: <ThunderstormRain /> },
  { id: [230, 231, 232], component: <ThunderStormDrizzle /> },
  { id: [300, 301, 302, 310, 311, 312, 313, 314, 321], component: <Drizzle /> },
  { id: [800], component: <Clear /> },
  { id: [500, 501, 502, 503, 504], component: <Rain /> },
  { id: [520, 521, 522, 531], component: <ShowerRain /> },
  { id: [511, 611, 612, 615, 616], component: <FreezingRain /> },
  { id: [601, 602], component: <Snow /> },
  { id: [600, 620, 621, 622], component: <ShowerSnow /> },
  { id: [701, 711, 721, 731, 741, 751, 761, 762, 771, 781], component: <Atmosphere /> },
  { id: [803, 804], component: <BrokenClouds /> },
  { id: [801], component: <FewClouds /> },
  { id: [802], component: <ScatteredClouds /> }
];

export default WEATHER_ICONS;
