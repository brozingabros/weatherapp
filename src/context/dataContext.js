import React, { createContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { TEMP_UNITS } from '../constants';

const DataContext = createContext([{}, () => {}]);

const DataProvider = ({ children, value }) => {
  const [state, setState] = useState({ ...value, currentUnit: TEMP_UNITS.celsius });
  useEffect(() => setState({ ...value, currentUnit: TEMP_UNITS.celsius }), [value]);

  return <DataContext.Provider value={[state, setState]}>{children}</DataContext.Provider>;
};

DataProvider.propTypes = {
  children: PropTypes.node.isRequired,
  value: PropTypes.shape({}).isRequired
};

export { DataContext, DataProvider };
