import React, { createContext, useState } from 'react';
import PropTypes from 'prop-types';

const UrlContext = createContext(['', () => {}]);

const UrlProvider = ({ children }) => {
  const [state, setState] = useState('');

  return <UrlContext.Provider value={[state, setState]}>{children}</UrlContext.Provider>;
};

UrlProvider.defaultProps = {
  value: {}
};

UrlProvider.propTypes = {
  children: PropTypes.node.isRequired,
  value: PropTypes.shape({})
};

export { UrlContext, UrlProvider };
