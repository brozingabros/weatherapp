import React, { useContext, useEffect } from 'react';
import { MuiThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import theme from './theme';
import { DataProvider, UrlContext } from './context';
import useFetch from './utils/hooks/useFetch';
import useCurrentLocation from './utils/hooks/useCurrentLocation';
import { API, FETCH_STATUS } from './constants';
import Home from './pages';
import { Bar, Layout } from './components';

function App() {
  const { location, error } = useCurrentLocation();
  const [url, setUrl] = useContext(UrlContext);

  useEffect(() => {
    if (error) {
      // error
    }
    if (location) {
      setUrl(`${API.url}?lat=${location.latitude}&lon=${location.longitude}&${API.units}&${API.key}`);
    }
  }, [error, location, setUrl]);

  const { response } = useFetch(url);

  if (response && response.status === FETCH_STATUS.error) return 'Error';
  return (
    <DataProvider value={response}>
      <MuiThemeProvider theme={theme}>
        <CssBaseline>
          <Layout>
            <Bar />
            <Home />
          </Layout>
        </CssBaseline>
      </MuiThemeProvider>
    </DataProvider>
  );
}

export default App;
