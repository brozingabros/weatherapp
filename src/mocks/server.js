import { setupServer } from 'msw/node';
import { rest } from 'msw';
import mockForecast from './forecast';
import { API } from '../constants';

// eslint-disable-next-line import/prefer-default-export
export const server = setupServer(
  rest.get(`${API.url}?${API.city_id}`, (req, res, ctx) => {
    return res(ctx.json(mockForecast));
  })
);
