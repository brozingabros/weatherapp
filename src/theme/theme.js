import { createMuiTheme } from '@material-ui/core/styles';
// colors import
import blueGrey from '@material-ui/core/colors/blueGrey';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: blueGrey[500], // #607d8b
      light: blueGrey[200], // #B0BEC5
      lighter: blueGrey[50] // #ECEFF1
    },
    secondary: {
      main: '#FFF'
    }
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 640,
      md: 960,
      lg: 1280,
      xl: 1920
    }
  },
  overrides: {
    MuiCssBaseline: {
      '@global': {
        button: {
          backgroundColor: 'transparent',
          border: 0,
          cursor: 'pointer',
          margin: 0,
          padding: 0
        },
        '#root': {
          display: 'flex',
          height: '100vh',
          width: '100vw'
        }
      }
    }
  }
});

export default theme;
