import React from 'react';
import { Box } from '@material-ui/core';
import { CurrentWeather, Forecast } from '../../components';

const Home = () => {
  return (
    <Box component='main' flexGrow='1'>
      <Box component='article' display='flex' flexDirection='column' height='100%' justifyContent='space-around'>
        <CurrentWeather />
        <Forecast />
      </Box>
    </Box>
  );
};

export default Home;
